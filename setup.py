from setuptools import find_packages, setup

setup(
    name='pyconde',
    version='0.3.0',
    packages=find_packages(),
    zip_safe=False,
)
